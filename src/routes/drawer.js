import React, { useEffect } from 'react';
import { TouchableOpacity, View, Text, Linking, Image } from 'react-native';
import { connect } from 'react-redux';
import { changeMenu } from '../services/actions'
import { Container, Content, Icon } from 'native-base';
import { ImageCircle } from '../views/components/image';
import { styles, colorsApp } from '../styles/style';

function DrawerPage(props) {

    const listMenu = [
        { name: 'home', icon: 'home' },
        { name: 'about', icon: 'person' },
        { name: 'portfolio', icon: 'today' },
        { name: 'work', icon: 'briefcase' },
        { name: 'tecnology', icon: 'bookmarks' }
    ];

    const listLink = [
        { icon: 'logo-chrome', link: 'http://erickmarlendo.com/' },
        { icon: 'logo-facebook', link: 'https://www.facebook.com/diangrafikaonline' },
        { icon: 'logo-whatsapp', link: 'https://api.whatsapp.com/send?phone=08999064808' },
        { icon: 'logo-instagram', link: 'https://www.instagram.com/erick_marlendo/?hl=id' },

    ]

    const drawerImage = require('../assets/images/background-drawer.jpg');
    const myImage = require('../assets/images/foto-gue.png');

    useEffect(() => {
        // console.log(props)
    }, [])

    function menuOpen(data) {
        props.closeDrawer();
        if (props.menu.menu.name !== data.name) {
            setTimeout(function () {
                props.Menu(data)
            }, 250);
        }
    }

    return (
        <Container>
            <Content>
                <View>
                    <Image
                        style={{
                            width: '100%',
                            height: 300,
                            position: 'absolute',
                            top: 0,
                            left: 0,
                        }}
                        source={drawerImage}
                    />
                    <View
                        style={[
                            {
                                height: 300,
                                paddingLeft: 20
                            },
                            styles.column
                        ]}
                    >
                        <View style={
                            [
                                styles.leftBottom,
                                { flex: 2 }
                            ]
                        }>
                            <ImageCircle
                                src={myImage}
                                width={100}
                            />
                        </View>
                        <View style={
                            [
                                styles.left,
                                { flex: 1 }
                            ]
                        }>
                            <Text style={styles.textDrawerTitle}>
                                Erick Marlendo Novianto
                                </Text>
                            <Text style={styles.textDrawerSubTitle}>
                                erikmarlendo@gmail.com
                                </Text>
                        </View>
                        <View style={
                            [
                                styles.left,
                                { flex: 1 }
                            ]
                        }>
                            <View style={[
                                styles.row,
                            ]}>
                                {listLink.map((row, i) => (
                                    <Icon
                                        onPress={() => {
                                            Linking.openURL(row.link)
                                        }}
                                        key={i.toString()}
                                        style={[
                                            styles.paddingSmall,
                                            { color: colorsApp.light }
                                        ]} name={row.icon} />
                                ))}
                            </View>
                        </View>
                    </View>
                </View>
                {listMenu.map((row, index) => (
                    <View style={{
                        backgroundColor: props.menu.menu.name === row.name ? '#E0E0E0' : '#FFFFFF'
                    }}>
                        <Menu
                            key={index.toString()}
                            icon={row.icon}
                            name={row.name}
                            action={() => {
                                menuOpen(row)
                            }} />
                    </View>
                ))}
            </Content>
        </Container>
    )
}

const Menu = (props) => {
    const state = props;
    return (
        <TouchableOpacity
            onPress={props.action}>
            <View style={[
                styles.row,
                styles.paddingSmall,
            ]}>
                <View style={[
                    { flex: 1 },
                    styles.center
                ]}
                >
                    <Icon
                        style={{
                            fontSize: 36,
                            color: '#616161'
                        }} name={state.icon} />
                </View>
                <View style={[
                    { flex: 3 },
                    styles.left
                ]}>
                    <Text style={{
                        fontWeight: 'bold',
                        color: '#424242',
                        textTransform: 'capitalize'
                    }}>
                        {state.name}
                    </Text>
                </View>
            </View>
        </TouchableOpacity>
    );
}

const mapStateToProps = state => {
    return {
        menu: state.store
    }
}

const mapDispatchToProps = dispatch => {
    return {
        Menu: (menu) => {
            dispatch(changeMenu(menu))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(DrawerPage);