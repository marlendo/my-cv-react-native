import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import HomePage from '../views/pages';
import TutorialPage from '../views/test/tutorialPage';
import Drawer from './drawer';

const MainNavigator = createStackNavigator({
  Home: { screen: HomePage },
  Tutorial: { screen: TutorialPage }
},{
  defaultNavigationOptions: {
    header: null
  }
});

const Main = createAppContainer(MainNavigator);
// const Main = Drawer;

export default Main;
