import { StyleSheet } from 'react-native';
import { Colors } from 'react-native/Libraries/NewAppScreen';

export const colorsApp = {
  statusBar: '#b71c1c',
  primary: '#f44336',
  secondary: '#E91E63',
  light: '#FFFFFF',
  grey: '#616161'
}

export const styles = StyleSheet.create({
  // container
  column: {
    flex: 1,
    flexDirection: 'column',
  },
  row: {
    flex: 1,
    flexDirection: 'row',
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  left: {
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  leftBottom: {
    justifyContent: 'flex-end',
    alignItems: 'flex-start',
  },
  container: {
    flex: 1,
  },
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  }, 
  // font
  textDrawerTitle: {
    fontWeight: 'bold',
    fontSize: 20,
    textTransform: 'capitalize',
    color: colorsApp.light
  },
  textDrawerSubTitle: {
    fontWeight: 'bold',
    fontSize: 16,
    color: colorsApp.light
  },
  textSubtitle: {
    fontWeight: 'bold',
    fontSize: 16,
  },  
  // button  
  buttonText: {
    textAlign: 'center',
    padding: 20,
    color: 'white'
  },
  // padding
  paddingSmall: {
    padding: 10
  },
  paddingNormal: {
    padding: 20
  }
});