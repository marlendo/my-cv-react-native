import React, { Component } from 'react';
import {
    Header,
    Drawer,
    Left,
    Body,
    Right,
    Button,
    Icon,
    Title,
    Text,
    Footer,
    Content
} from 'native-base';

import DrawerComponent from '../../routes/drawer';

import { styles, colorsApp } from '../../styles/style';
import { connect } from 'react-redux';
import HomePage from './home';

function RootPage(props) {

    const { navigate } = props.navigation;
    let drawer;

    function closeDrawer() {
        drawer._root.close()
    };

    return (
        <Drawer
            type={'displace'}
            ref={(ref) => {
                drawer = ref;
            }}
            content={
                <DrawerComponent
                    closeDrawer={() => {
                        closeDrawer()
                    }} />
            }
        >
            <Header 
                androidStatusBarColor={colorsApp.statusBar} style={{
                backgroundColor: colorsApp.primary
            }}>
                <Left>
                    <Button transparent onPress={() => {
                        drawer._root.open();
                    }}>
                        <Icon name='menu' />
                    </Button>
                </Left>
                <Body>
                    <Title style={styles.textDrawerTitle}>{props.menu.name}</Title>
                </Body>
                <Right />
            </Header>
            <Content>               
                <HomePage />
                <Text>
                    {JSON.stringify(props.menu)}
                </Text>
            </Content>
            <Footer>
                <Button style={{
                    width: '100%',
                    height: '100%',
                    backgroundColor: colorsApp.primary
                }} onPress={() => {
                    navigate('Tutorial')
                }}>
                    <Text style={{
                        textAlign: 'center',
                        width: '100%'
                    }}>Tutorial</Text>
                </Button>
            </Footer>
        </Drawer>
    )
}

const mapStateToProps = state => {
    return {
        menu: state.store.menu
    }
}


export default connect(mapStateToProps)(RootPage);
