import React from 'react';
import { View, Image, Dimensions, Text } from 'react-native';
import { MyCard } from '../components/card';
import { styles, colorsApp } from '../../styles/style';
import { ImageCircle } from '../components/image';
import { H1 } from 'native-base';
import { Space } from '../components/space';

const imgBG = require('../../assets/images/background-home.png');
const myImage = require('../../assets/images/foto-gue.png');
const dWidth = Dimensions.get('screen').width;

function HomePage() {
    return (
        <View style={styles.paddingSmall}>
            <MyCard>
                <Image
                    resizeMode={'cover'}
                    style={{
                        width: '100%',
                        height: 200
                    }}
                    source={imgBG}
                />
                <View style={[
                    styles.paddingSmall,
                    styles.row

                ]}>
                    <View style={[
                        { flex: 1 },
                        styles.left
                    ]}>
                        <ImageCircle width={dWidth / 4} src={myImage} />
                    </View>
                    <View
                        style={[
                            { 
                                flex: 2, 
                            },
                            styles.left,
                        ]}>
                            <H1>Erick Marlendo</H1>
                            <Text style={[
                                styles.textSubtitle,
                                { color: colorsApp.grey }
                                ]}>Fullstack Javascript Developer</Text>
                    </View>
                </View>
            </MyCard>
        </View>
    )
}

export default HomePage;