import React, { useState } from 'react';
import { StyleSheet, View, TextInput, Button, FlatList } from 'react-native';
import ListItemRedux from './components/ListItem';
import { connect } from 'react-redux';
import { addPlace } from './actions/place';

function App(props) {

    // const [places, setPlaces] = useState([]);
    const [placeName, setPlaceName] = useState('');

    function placeSubmitHandler() {
        if (placeName.trim() === '') {
            return;
        }
        props.add(placeName);
    }

    function placeNameChangeHandler(value) {
        setPlaceName(value)
    }

    return (
        <View style={styles.container}>
            <View style={styles.inputContainer}>
                <TextInput
                    placeholder="Seach Places"
                    style={styles.placeInput}
                    value={placeName}
                    onChangeText={placeNameChangeHandler}
                ></TextInput>
                <Button title='Add'
                    style={styles.placeButton}
                    onPress={placeSubmitHandler}
                />
            </View>
            <View style={styles.listContainer}>
                <ListItemRedux />
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        paddingTop: 30,
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    inputContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%'
    },
    placeInput: {
        width: '70%'
    },
    placeButton: {
        width: '30%'
    },
    listContainer: {
        width: '100%'
    }
});

const mapStateToProps = state => {
    return {
        places: state.places.places
    }
}

const mapDispatchToProps = dispatch => {
    return {
        add: (name) => {
            dispatch(addPlace(name))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)