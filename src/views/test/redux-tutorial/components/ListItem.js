import React, { useEffect } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, FlatList } from 'react-native';
import { connect } from 'react-redux';

function ListItemRedux(props) {

    useEffect(() => {
        console.log('now')
    }, [])

    return (
        <FlatList
            style={styles.listContainer}
            data={props.places}
            keyExtractor={(item, index) => index.toString()}
            renderItem={info => (
                <TouchableOpacity>
                    <View style={styles.listItem}>
                        <Text>{info.item.value}</Text>
                    </View>
                </TouchableOpacity>
            )}
        />
    )
}

const mapStateToProps = state => {
    return {
        places: state.places.places
    }
}

const styles = StyleSheet.create({
    listItem: {
        width: '100%',
        padding: 10,
        marginBottom: 10,
        backgroundColor: '#eee'
    }
});

export default connect(mapStateToProps)(ListItemRedux);