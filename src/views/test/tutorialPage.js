import React from 'react';

import {
    Header,
    Left,
    Body,
    Right,
    Button,
    Icon,
    Title
} from 'native-base';

import {
    SafeAreaView,
    ScrollView,
    View,
    StatusBar,
} from 'react-native';

import {
    Header as Jonson,
} from 'react-native/Libraries/NewAppScreen';

import { styles } from '../../styles/style';
import Tutorial from './tutorial';

function TutorialPage() {
    return (
        <>
            <StatusBar barStyle="light-content" />
            <SafeAreaView>
                <Header>
                    <Left>
                        <Button transparent>
                            <Icon name='menu' />
                        </Button>
                    </Left>
                    <Body>
                        <Title>Home</Title>
                    </Body>
                    <Right />
                </Header>
                <ScrollView
                    contentInsetAdjustmentBehavior="automatic"
                    style={styles.scrollView}>
                    <Jonson />
                    <View style={styles.body}>
                        <Tutorial />
                    </View>
                </ScrollView>
            </SafeAreaView>
        </>
    );
};

export default TutorialPage;
