import React from "react";
import { View } from "react-native";

export function Space(props) {
    return (
        <View
            style={{
                width: '100%',
                height: props.height ? props.height : 10
            }}
        />
    );
}