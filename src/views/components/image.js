import React from 'react';
import { Image } from 'react-native';

export function ImageCircle(props) {
    const width = props.width;
    const image = props.src;
    return (
        <Image
            resizeMode={'cover'}
            style={{
                width: width,
                height: width,
                borderRadius: width / 2,
            }}
            source={image}
        />
    );
}

// export function ImageFull(props) {
//     const width = props.width;
//     const image = props.src;
//     return (
//         <Image
//             resizeMode={'cover'}
//             style={{
//                 width: width,
//                 height: '100%',
//             }}
//             source={image}
//         />
//     );
// }