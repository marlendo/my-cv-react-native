import { MENU } from '../actions/types';

const initialState = {
  menu: { name: 'home', icon: 'home' },
};

const Reducer = (state = initialState, action) => {
  switch(action.type) {
    case MENU:
      return {
        ...state,
        menu: action.payload
      };
    default:
      return state;
  }
}

export default Reducer;