import { MENU } from './types';

export const changeMenu = menu => {
    return {
        type: MENU,
        payload: menu
    }
}