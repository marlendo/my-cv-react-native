import React from 'react';
import Main from './src/routes/main';
import { Provider } from 'react-redux';
import configureStore from './src/services/store';

const store = configureStore()

function App() {
  return (
    <Provider store={store} >
      <Main />
    </Provider>
  );
};

export default App;
